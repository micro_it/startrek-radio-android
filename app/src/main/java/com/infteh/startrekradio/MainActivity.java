package com.infteh.startrekradio;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.infteh.startrekplayer.StartrekAndroid;
import com.infteh.startrekplayer.StartrekMetadata;
import com.infteh.startrekplayer.StartrekMetadataFetchType;
import com.infteh.startrekplayer.StartrekMetadataSubscriptionType;
import com.infteh.startrekplayer.StartrekMetadataWatcher;
import com.infteh.startrekplayer.StartrekMetadataWatcherDelegate;
import com.infteh.startrekplayer.StartrekNetwork;
import com.infteh.startrekplayer.StartrekPlayer;
import com.infteh.startrekplayer.StartrekPlayerDelegate;
import com.infteh.startrekplayer.StartrekPlayerQuality;
import com.infteh.startrekplayer.StartrekPlayerState;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    static {
        System.loadLibrary("StartrekPlayerNative" + StartrekAndroid.PREFERRED_ABI);
    }

    StartrekMetadataWatcher m_meta = null;
    StartrekPlayer m_player = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StartrekNetwork.setCaCertificates(StartrekAndroid.getSSLCertificates());
        StartrekNetwork.setReferer("com.infteh.startrekradio.android");

        m_meta = StartrekMetadataWatcher.create();
        m_meta.setUrl("wss://metadataws.hostingradio.ru");

        m_player = StartrekPlayer.create();
        m_player.setDelegate(new StartrekPlayerDelegate() {
            @Override
            public void ended() {
                Log.d("PLAYER", "ended");
            }

            @Override
            public void error(String message) {
                Log.d("PLAYER", "error" + message);
            }

            @Override
            public void streamUrlChanged(String streamUrl) {
                Log.d("PLAYER", "streamUrlChanged " + streamUrl);
            }

            @Override
            public void isRestartedChanged(boolean isRestarted) {
                Log.d("PLAYER", "isRestartedChanged " + isRestarted);
            }

            @Override
            public void isHlsChanged(boolean isHls) {
                Log.d("PLAYER", "isHlsChanged " + isHls);
            }

            @Override
            public void isSeekableChanged(boolean isSeekable) {
                Log.d("PLAYER", "isSeekableChanged " + isSeekable);
            }

            @Override
            public void stateChanged(StartrekPlayerState state) {
                Log.d("PLAYER", "stateChanged " + state);
            }

            @Override
            public void isPlayingChanged(boolean isPlaying) {
                Log.d("PLAYER", "ended");
            }

            @Override
            public void isStalledChanged(boolean isStalled) {
                Log.d("PLAYER", "isStalledChanged " + isStalled);
            }

            @Override
            public void isPausedChanged(boolean isPaused) {
                Log.d("PLAYER", "isPausedChanged " + isPaused);
            }

            @Override
            public void isStoppedChanged(boolean isStopped) {
                Log.d("PLAYER", "isStoppedChanged " + isStopped);
            }

            @Override
            public void lengthChanged(double length) {
                Log.d("PLAYER", "lengthChanged " + length);
            }

            @Override
            public void bufferedLengthChanged(double bufferedLength) {
                Log.d("PLAYER", "bufferedLengthChanged " + bufferedLength);
            }

            @Override
            public void startPositionChanged(double startPosition) {
                Log.d("PLAYER", "startPositionChanged " + startPosition);
            }

            @Override
            public void positionChanged(double position) {
                Log.d("PLAYER", "positionChanged " + position);
            }

            @Override
            public void playbackRateChanged(double playbackRate) {
                Log.d("PLAYER", "playbackRateChanged " + playbackRate);
            }

            @Override
            public void metaChanged(String meta) {
                Log.d("PLAYER", "metaChanged " + meta);
            }

            @Override
            public void volumeChanged(double volume) {
                Log.d("PLAYER", "volumeChanged " + volume);
            }

            @Override
            public void duckVolumeChanged(boolean duckVolume) {
                Log.d("PLAYER", "duckVolumeChanged " + duckVolume);
            }

            @Override
            public void playingBitratesChanged(ArrayList<Integer> playingBitrates) {
                Log.d("PLAYER", "playingBitratesChanged " + playingBitrates);
            }

            @Override
            public void availableBitratesChanged(ArrayList<Integer> availableBitrates) {
                Log.d("PLAYER", "availableBitratesChanged " + availableBitrates);
            }

            @Override
            public void currentBitrateChanged(int currentBitrate) {
                Log.d("PLAYER", "currentBitrateChanged " + currentBitrate);
            }

            @Override
            public void currentQualityChanged(StartrekPlayerQuality currentQuality) {
                Log.d("PLAYER", "currentQualityChanged " + currentQuality);
            }

            @Override
            public void playingBitrateChanged(int playingBitrate) {
                Log.d("PLAYER", "playingBitrateChanged " + playingBitrate);
            }

            @Override
            public void playingQualityChanged(StartrekPlayerQuality playingQuality) {
                Log.d("PLAYER", "playingQualityChanged " + playingQuality);
            }

            @Override
            public void daastUrlChanged(String daastUrl) {
                Log.d("PLAYER", "daastUrlChanged " + daastUrl);
            }

            @Override
            public void daastStarted(String meta, String imageUrl, String clickUrl) {
                Log.d("PLAYER", "daastStarted " + meta + " " + imageUrl + " " + clickUrl);
            }

            @Override
            public void daastError(String message) {
                Log.d("PLAYER", "daastError " + message);
            }

            @Override
            public void daastSkipped() {
                Log.d("PLAYER", "daastSkipped");
            }

            @Override
            public void daastEnded() {
                Log.d("PLAYER", "daastEnded");
            }
        });

//        m_player.setDaastUrl("http://a.adwolf.ru/2909/getCode?pp=do&ps=clc&p2=jg");
        m_player.playUrl("https://hls-02-europaplus.emgsound.ru/11/playlist.m3u8");

        m_meta.setDelegate(new StartrekMetadataWatcherDelegate() {
            @Override
            public void connected() {
                Log.d("META", "connected");
                m_meta.fetchOne("ep", StartrekMetadataFetchType.FULL_FETCH);
            }

            @Override
            public void disconnected() {
                Log.d("META", "disconnected");
            }

            @Override
            public void metadata(String stationId, StartrekMetadata current, ArrayList<StartrekMetadata> next, ArrayList<StartrekMetadata> previous) {
                Log.d("META", "Station" + stationId + "current" + current);
            }
        });

        m_meta.subscribeOne("dr", StartrekMetadataSubscriptionType.CURRENT_SUBSCRIPTION);
        m_meta.fetchOne("ep", StartrekMetadataFetchType.FULL_FETCH);
    }
}
